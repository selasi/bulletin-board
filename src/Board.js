import React, { Component } from 'react'
import Note from './Note'
import { FaPlus } from 'react-icons/fa'

class Board extends Component {
	constructor(props){
		super(props)
		this.state = {
			notes: []
		}
		this.add = this.add.bind(this)
		this.eachNote = this.eachNote.bind(this)
		this.update = this.update.bind(this)
		this.remove = this.remove.bind(this)
		this.nextId = this.nextId.bind(this)
	}

	componentWillMount () {
		var self = this
		if(this.props.count) {
			fetch(`https://baconipsum.com/api/?type=all-meat&sentences=${this.props.count}`)
				.then(response => response.json())
				.then(json => json[0]
								.split('. ')
								.forEach(sentence => self.add(sentence.substring(0, 25))))
		}
	}

	// Select & Focus of the teatarea if editing is true
	componentDidUpdate () {
		var textArea
		if(this.state.editing) {
			textArea = this._newText
			textArea.focus()
			textArea.select()
		}
	}

	// Check to see if something has been changed before component re-renders, if not there will be no re-render
	shouldComponentUpdate (nextProps, nextState) {
		return (
		 	this.props.children !== nextProps.children || this.state !== nextState
		)
	}

	add (text) {
		this.setState(prevState => ({
			notes: [
				...prevState.notes,
				{
					id: this.nextId(),
					note: text
				}
			]
		}))
	}

	nextId() {
		this.uniqueId = this.uniqueId || 0
		return this.uniqueId++
	}

    update (newText, i) {
    	console.log('Updating item at index ', i, newText)
    	this.setState(prevState =>({
    		notes: prevState.notes.map(
    			note => (note.id !== i) ? note : {...note, note: newText}
    		)
    	}))
    }
    remove (id) {
        console.log('removing id ', id)
        this.setState(prevState => ({
        	notes: prevState.notes.filter(note => note.id !== id)
        }))
    }

	eachNote (note, i){
		return (
			<Note key={note.id} index={note.id} onChange={this.update} onRemove={this.remove}>
				{note.note}
			</Note>
		)
	}

	render () {
		return (
			<div className="board">
				{this.state.notes.map(this.eachNote)}
				<button onClick={this.add.bind(null, "New Note")} id="add">
					<FaPlus />
				</button>
			</div>
		)
	}
}

export default Board